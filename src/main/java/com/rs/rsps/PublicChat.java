package com.rs.plugins;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.rs.game.World;
import com.rs.game.model.entity.player.Player;
import com.rs.lib.game.Rights;
import com.rs.plugin.annotations.PluginEventHandler;
import com.rs.plugin.annotations.ServerStartupEvent;
import com.rs.engine.command.Commands;
import com.rs.plugin.handlers.LoginHandler;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;

@PluginEventHandler
public class PublicChat {
    private static WebSocketClient websocket;
    private static String access_token;
    @ServerStartupEvent
    public static void serverStartup() {
        login("public chat", "publicchat");
        Commands.add(Rights.PLAYER, "fc [message]", "Logs in a player into Darkan Friends Chat.", (p, args) -> {
            sendMessage(p, String.join(" ", args));
        });
    }

    public static LoginHandler onLogin = new LoginHandler(e -> {
        if(websocket == null || websocket.isClosed())
            e.getPlayer().sendMessage("<col=800000>Darkan chat closed!");
        else
            e.getPlayer().sendMessage("<col=00FFFF>Darkan connection open!");
    });


    private static void sendMessage(Player player, String message) {
        if(websocket != null && websocket.isOpen()) {
            String srcMessage = player.getDisplayName() + " says -> " + message;
            websocket.send("{\"event\":\"fc-message\",\"data\":{\"message\":\"" + srcMessage + "\"}}");
        }
        else
            player.sendMessage("<col=FF0000>WebSocket connection is not open.");
    }

    private static void login(String username, String password) {
        HttpClient httpClient = HttpClient.newBuilder().build();
        String loginUrl = "https://api.darkan.org/v1/accounts/login";
        String requestBody = "{\"username\":\"" + username + "\",\"password\":\"" + password + "\"}";

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(loginUrl))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == 201) {
                String responseBody = response.body();
                // Parse the JSON string
                JsonParser parser = new JsonParser();
                JsonObject responseJson = parser.parse(responseBody).getAsJsonObject();

                // Parse the JSON response to get the access token
                access_token = responseJson.get("access_token").getAsString();
                connectWebSocket();
            } else {
                System.out.println("Login failed. Status code: " + response.statusCode());
            }
        } catch (IOException | InterruptedException e) {
            System.out.println("Error occurred during login: " + e.getMessage());
        }
    }

    private static void connectWebSocket() {
        String wsUri = "wss://api.darkan.org/v1/social/ws?token=" + access_token;

        try {
            websocket = new WebSocketClient(new URI(wsUri)) {
                @Override
                public void onOpen(ServerHandshake handshakedata) {
                    ;
                }

                @Override
                public void onMessage(String message) {
                    JsonParser parser = new JsonParser();
                    JsonObject responseJson = parser.parse(message).getAsJsonObject().get("data").getAsJsonObject();
                    //System.out.println(responseJson);
                    if(!responseJson.has("srcName") || !responseJson.has("message"))
                        return;
                    String srcName = responseJson.get("srcName").getAsString();
                    String srcMessage = responseJson.get("message").getAsString();
                    World.getPlayers().stream().forEach(player -> {
                        player.sendMessage("[<col=00FFFF>Darkan<col=FFFFFF>] " + srcName + " <col=00FF00>" + srcMessage);
                    });
                }

                @Override
                public void onClose(int code, String reason, boolean remote) {
                    System.out.println("Darkan chat closed");
                    World.getPlayers().stream().forEach(player -> {
                        player.sendMessage("<col=800000>Darkan chat closed!");
                    });
                }

                @Override
                public void onError(Exception ex) {
                    System.out.println("Darkan chat closed! Error: " + ex.getMessage());
                    World.getPlayers().stream().forEach(player -> {
                        player.sendMessage("<col=FF0000>Darkan chat closed! Error: " + ex.getMessage());
                    });
                }
            };

            websocket.connect();
        } catch (URISyntaxException e) {
            System.out.println("Invalid WebSocket URI: " + e.getMessage());
        }
    }

}